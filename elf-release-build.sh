#!/bin/bash

#--------watch app---------#
PROJECTS="
AmazingCalendarProvider
AmazingCalendar
AmazingCountdown
AmazingHealth
AmazingLauncher
AmazingTimer
AmazingAlarmClock
AmazingNotification
AmazingNotificationProvider
AmazingWeather
AmazingCalculator
AmazingSoundRecorder
AmazingMusic
AmazingTheme
AmazingSms
AmazingRemoteCamera
"

echoc()
{
  echo -e "\e[0;92m$1\e[0m"
}

echoc "Compiling apk ..."
ANT_PROJ="ant project"
which busybox > /dev/null && BUSYBOX="busybox" # `realpath' not default installed on many distribution
top_dir="$($BUSYBOX realpath "$(dirname "${BASH_SOURCE[0]}")/..")"

cd "$top_dir"

# check env, TODO: recheck the checking method
if [[ "$NDK_HOME" == "" || "$ANDROID_HOME" == "" ]]; then
    echo '
environment check error, FYR:

  export NDK_HOME=<path-to-android-ndk>
  export ANDROID_HOME=<path-to-android-sdk>

can be added to your profile or .bashrc
'
    exit 1
fi

# check build command
if [[ "$1" != "release" && "$1" != "debug" &&  "$1" != "clean" && "$1" != "force" && "$1" != "help" ]]; then
echo '
  command error,please execute the command correctly.
  example:
  ./AntBuildConfig/Antbuild.sh clean
  ./AntBuildConfig/Antbuild.sh debug
  ./AntBuildConfig/Antbuild.sh release
  ./AntBuildConfig/Antbuild.sh help
'
    exit 1
fi

# help
if [[ "$1" == help ]]; then
echo '
  ./AntBuildConfig/Antbuild.sh clean :       Removes output files created by other targets.
  ./AntBuildConfig/Antbuild.sh debug :       Builds the application in debug mode.
  ./AntBuildConfig/Antbuild.sh release :     Builds the application in release mode.
  ./AntBuildConfig/Antbuild.sh help :        help
'
   exit 1;
fi

# clean, TODO: warning before clean, or do the cleanup work using the smk only
if [ "$1" == "force" ]; then
  echoc "start repo sync elf source."
  repo sync > /dev/null
  repo forall -c 'git reset --hard' > /dev/null
  repo forall -c 'git clean -dxf' > /dev/null
  echoc "repo sync done."
fi

# clean
if [ "$1" == "clean" ]; then
  echoc "Ant clean begining"
  for proj in $PROJECTS; do
     cd "$top_dir"
     echoc "Ant clean $proj"

     cd $proj/ || { echoc "directory $proj does not exist"; exit 1; }
     ant clean || { echoc "failed to ant clean $proj"; exit 1; }
     echo ""
  done
  echoc "Ant clean end."
  exit 0
fi


export top_pid=$$
function stop_all_job {
  echo "$0 kill all background jobs before exit, pid=$top_pid"
  pkill -9 -P $top_pid
  echo "$0 exit..."
  exit 1
}
trap stop_all_job INT TERM EXIT

# build
buildmode=$1
rm -rf "$top_dir/.tmp"
mkdir "$top_dir/.tmp"
[[ "$MP" == "" ]] && MP=5

for proj in $PROJECTS; do

  while true; do
    (( $(jobs -p | wc -l) < MP )) && break
    sleep 1
  done

  cd "$top_dir"
  echoc "Ant Compiling $proj"

  proj_dir=$proj
  #delete old iwds jar & so
  echoc "delete old .jar, copy new file to $proj libs"
  mkdir -p $proj/libs
  rm -vf $proj/libs/iwds*.jar
  cp -vrf JavaDocAndBuildOut/iwds-api*/user/libs/*   $proj/libs   || { echoc "failed to copy libs file"; kill $top_pid; exit 1; }
  if [[ "$proj" == "AmazingLauncher" ]]; then
     cp -vf AntBuildConfig/project-launcher.properties $proj/project.properties || { echoc "failed to copy file"; kill $top_pid; exit 1; }
     if [[ -d "$proj/AmazingClock" ]]; then
        echoc "delete $proj/AmazingClock"
        rm -rf $proj/AmazingClock
     fi
     echoc "cp AmazingClock to $proj"
     cp -rf AmazingClock $proj/.
     rm -rf $proj/AmazingClock/.git

     if [[ -d "$proj/iwds-ui-res-jar" ]]; then
        echoc "delete $proj/iwds-ui-res-jar"
        rm -rf $proj/iwds-ui-res-jar
     fi
     echoc "cp iwds-ui-res-jar to $proj"
     cp -rf iwds-ui-res-jar  $proj/.
     rm -rf $proj/iwds-ui-res-jar/.git

  else
     cp -vf AntBuildConfig/project-use-res.properties  $proj/project.properties || { echoc "failed to copy file"; kill $top_pid; exit 1; }
     if [[ -d "$proj/iwds-ui-res-jar" ]]; then
        echoc "delete $proj/iwds-ui-res-jar"
        rm -rf $proj/iwds-ui-res-jar
     fi
     echoc "cp iwds-ui-res-jar to $proj"
     cp -rf iwds-ui-res-jar  $proj/.
     rm -rf $proj/iwds-ui-res-jar/.git
  fi
  #copy ingenic.keystore
  if [[ -d "keystore" ]]; then
     echoc "copy keystore to $proj project"
     cp -vf keystore/ingenic.keystore   $proj/ingenic.keystore || { echoc "failed to copy file"; kill $top_pid; exit 1; }
     cp -vf keystore/ant.properties     $proj/ant.properties   || { echoc "failed to copy file"; kill $top_pid; exit 1; }
  fi

  cd $proj/ || { echoc "directory $proj does not exist"; kill -SIGTERM $top_pid; }
  if [[ $buildmode == "debug" ]]; then
    ant elf-debug > "$top_dir/.tmp/$proj.log" 2>&1 || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
    echoc "  compile $proj done"
  else
    ant elf-release > "$top_dir/.tmp/$proj.log" 2>&1 || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
    echoc "  compile $proj done"
  fi &
done

echoc "wait......."
wait

cd "$top_dir/.tmp/"
for f in *; do
  echoc "dump $f:"
  cat "$f"
  echo ""
done

trap - INT TERM EXIT
echoc "$0 done."

