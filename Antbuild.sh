#!/bin/bash

#--------watch app---------#
PROJECTS="
iwds-jar
iwds
iwds-ui-with-res-jar
iwds-ui-jar
"
SERVER_PROJECTS="
iwds-device
watch-manager
"

echoc()
{
  echo -e "\e[0;92m$1\e[0m"
}

export top_pid=$$
function stop_all_job {
  echo "$0 kill all background jobs before exit, pid=$top_pid"
  pkill -9 -P $top_pid
  echo "$0 exit..."
  exit 1
}
trap stop_all_job INT TERM EXIT

makejavadoc()
{
    cd "$top_dir"
    rm -rf iwds-doc
    mkdir  iwds-doc
    cp -rf iwds-ui-res-jar iwds-doc/.
    cp -rf iwds-jar/.    iwds-doc/.
    cp -rf iwds-ui-jar/. iwds-doc/.
    cp -vf  AntBuildConfig/javadoc_build.xml           iwds-doc/build.xml
    cp -vf  AntBuildConfig/javadoc_custom_rules.xml    iwds-doc/custom_rules.xml
    cp -vf  AntBuildConfig/project-use-res.properties  iwds-doc/project.properties
    cd  iwds-doc
    ant iwds-doc || { echoc "failed to makejavadoc"; exit 1; }
    jar vcf  iwds-javadoc.jar doc/*
    cp  -vf iwds-javadoc.jar ../"$doc_path"
    cp  -rf doc  ../"$doc_path"
    cd "$top_dir"
}
echoc "Compiling apk ..."
ANT_PROJ="ant project"
which busybox > /dev/null && BUSYBOX="busybox" # `realpath' not default installed on many distribution
top_dir="$($BUSYBOX realpath "$(dirname "${BASH_SOURCE[0]}")/..")"

cd "$top_dir"

# check env, TODO: recheck the checking method
if [[ "$NDK_HOME" == "" || "$ANDROID_HOME" == "" ]]; then
    echo '
environment check error, FYR:

  export NDK_HOME=<path-to-android-ndk>
  export ANDROID_HOME=<path-to-android-sdk>

can be added to your profile or .bashrc
'
    exit 1
fi
# check build command
if [[ "$1" != "release" && "$1" != "debug" &&  "$1" != "clean" && "$1" != "force" && "$1" != "help" ]]; then
echo '
  command error,please execute the command correctly.
  example:
  ./AntBuildConfig/Antbuild.sh clean
  ./AntBuildConfig/Antbuild.sh debug
  ./AntBuildConfig/Antbuild.sh release
  ./AntBuildConfig/Antbuild.sh help
'
    exit 1
fi

# help
if [[ "$1" == help ]]; then
echo '
  ./AntBuildConfig/Antbuild.sh clean :       Removes output files created by other targets.
  ./AntBuildConfig/Antbuild.sh debug :       Builds the application in debug mode.
  ./AntBuildConfig/Antbuild.sh release :     Builds the application in release mode.
  ./AntBuildConfig/Antbuild.sh help :        help
'
   exit 1;
fi

if [[ ! -d iwds ]]; then
  echo '
  The project is missing iwds iwds-jar iwds-ui-jar iwds-ui-with-res-jar source code
  please execute the follow command:
  example:
  ./AntBuildConfig/elf-release-build.sh clean
  ./AntBuildConfig/elf-release-build.sh debug
  ./AntBuildConfig/elf-release-build.sh release
  ./AntBuildConfig/elf-release-build.sh help
'
  exit 1;
fi

# clean, TODO: warning before clean, or do the cleanup work using the smk only
if [ "$1" == "force" ]; then
  echoc "start repo sync elf source."
  repo sync > /dev/null
  repo forall -c 'git reset --hard' > /dev/null
  repo forall -c 'git clean -dxf' > /dev/null
  echoc "repo sync done."
fi

# clean
if [ "$1" == "clean" ]; then
  echoc "Ant clean begining"
  rm -rf JavaDocAndBuildOut/*
  for proj in $PROJECTS; do
     cd "$top_dir"
     echoc "Ant clean $proj"

     cd $proj/ || { echoc "directory $proj does not exist"; exit 1; }
     ant clean || { echoc "failed to ant clean $proj"; exit 1; }
     echo ""
  done
  for proj in $SERVER_PROJECTS; do
     cd "$top_dir"
     echoc "Ant clean $proj"

     cd $proj/ || { echoc "directory $proj does not exist"; exit 1; }
     ant clean || { echoc "failed to ant clean $proj"; exit 1; }
     echo ""
  done
  cd "$top_dir"
  ./AntBuildConfig/elf-release-build.sh $1
  echoc "Ant clean end."
  exit 0
fi
rm -rf JavaDocAndBuildOut/*
#get tag and git commit time
cd $top_dir/iwds-jar
last_tag=$(git describe --abbrev=0 --tags)
iwds_jar_commit_date=$(git show -s --format="%ci" HEAD)  # format: 2015-05-27 10:40:13 +0800
iwds_jar_commit_date=$(date -d "$iwds_jar_commit_date" +%Y%m%d_%H%M)  # format: 20150527_1040
echoc "iwds_jar_commit_date=:$iwds_jar_commit_date tag=:$last_tag"
if [[ "$last_tag" != "" ]] ; then
   [[ "$(git name-rev --tags --name-only HEAD)" != "undefined" ]] && iwds_jar_is_on_tag=true || iwds_jar_is_on_tag=false
fi

cd "$top_dir"/iwds
last_tag=$(git describe --abbrev=0 --tags)
iwds_commit_date=$(git show -s --format="%ci" HEAD)  # format: 2015-05-27 10:40:13 +0800
iwds_commit_date=$(date -d "$iwds_commit_date" +%Y%m%d_%H%M)  # format: 20150527_1040
echoc "iwds_commit_date=:$iwds_commit_date tag=:$last_tag"
if [[ "$last_tag" != "" ]] ; then
   [[ "$(git name-rev --tags --name-only HEAD)" != "undefined" ]] && iwds_is_on_tag=true || iwds_is_on_tag=false
fi

cd "$top_dir"/iwds-ui-with-res-jar
last_tag=$(git describe --abbrev=0 --tags)
iwds_ui_res_commit_date=$(git show -s --format="%ci" HEAD)  # format: 2015-05-27 10:40:13 +0800
iwds_ui_res_commit_date=$(date -d "$iwds_ui_res_commit_date" +%Y%m%d_%H%M)  # format: 20150527_1040
echoc "iwds_ui_res_commit_date=:$iwds_ui_res_commit_date tag=:$last_tag"
if [[ "$last_tag" != "" ]] ; then
   [[ "$(git name-rev --tags --name-only HEAD)" != "undefined" ]] && iwds_ui_res_is_on_tag=true || iwds_ui_res_is_on_tag=false
fi

cd "$top_dir"/iwds-ui-jar
last_tag=$(git describe --abbrev=0 --tags)
iwds_ui_jar_commit_date=$(git show -s --format="%ci" HEAD)  # format: 2015-05-27 10:40:13 +0800
iwds_ui_jar_commit_date=$(date -d "$iwds_ui_jar_commit_date" +%Y%m%d_%H%M)  # format: 20150527_1040
echoc "iwds_ui_jar_commit_date=:$iwds_ui_jar_commit_date tag=:$last_tag"
if [[ "$last_tag" != "" ]] ; then
   [[ "$(git name-rev --tags --name-only HEAD)" != "undefined" ]] && iwds_ui_jar_is_on_tag=true || iwds_ui_jar_is_on_tag=false
fi

if [[ "$iwds_jar_is_on_tag" == "true" && "$iwds_is_on_tag" == "true" && "$iwds_ui_res_is_on_tag" == "true" && "$iwds_ui_jar_is_on_tag" == "true" ]] ; then
   all_is_on_tag=true
else
   commit_date_last=$(echo -e "$iwds_jar_commit_date\n$iwds_commit_date\n$iwds_ui_res_commit_date\n$iwds_ui_jar_commit_date" | sort | tail -1)
fi

if [[ "$last_tag" == "" ]]; then
   last_tag="aaa-bbb-ccc-test-api1"
fi
cd "$top_dir"
# build
buildmode=$1
#check git tag ,return if the "api" string is not exist;
#[[ ! "$last_tag" =~ "api" ]] && { echoc "The tag error,it contains no api strings,please check it. \nCorrect tag is for example:iwop-elfos-v0.9(alpha0)-api1"; exit 1;}

iwds_api_level=${last_tag/*api}
if [[ "$iwds_api_level" == "" ]]; then
   iwds_api_level="1"
fi
echoc "iwds api level is "$iwds_api_level""

if [[ "$all_is_on_tag" == "true" ]]; then
   iwds_api_path=iwds-api$iwds_api_level-$buildmode
else
   iwds_api_path=iwds-api$iwds_api_level-$commit_date_last-$buildmode
fi
echoc "iwds api out path $iwds_api_path"
iwds_api_outpath=JavaDocAndBuildOut/$iwds_api_path
serverlibs="$iwds_api_outpath"/server/libs
userlibs="$iwds_api_outpath"/user/libs
doc_path="$iwds_api_outpath"/doc
mkdir -p $serverlibs
mkdir -p $userlibs
mkdir -p $doc_path

for proj in $PROJECTS; do
  cd "$top_dir"
  echoc "Ant Compiling $proj in $buildmode mode"

  cd $proj/ || { echoc "directory $proj does not exist"; exit 1; }
  if [[ $buildmode == "debug" ]]; then
    ant elf-debug   || { echoc "failed to compile $proj"; exit 1; }
    if [[ "$proj" == "iwds-jar" ]]; then
       cp -vf bin/classes.jar             ../$serverlibs/$proj.jar  || { echoc "failed to cp "$proj".jar"; exit 1; }
       cp -vf bin/classes.jar             ../$userlibs/$proj.jar    || { echoc "failed to cp "$proj".jar"; exit 1; }
    elif [[ "$proj" == "iwds" ]]; then
       cp -vf bin/classes.jar             ../$serverlibs/$proj.jar  || { echoc "failed to cp "$proj".jar"; exit 1; }
       cp -vrf libs/arm*                  ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
       cp -vrf libs/mips*                 ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
       cp -vrf libs/x86*                  ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
    else
       cp -vf bin/classes.jar             ../$userlibs/$proj.jar    || { echoc "failed to cp "$proj".jar"; exit 1; }
    fi
  else
    ant elf-release || { echoc "failed to compile $proj"; exit 1; }
    if [[ $proj == "iwds-ui-with-res-jar" ]]; then
        cp -vf bin/classes.jar             ../$userlibs/$proj.jar  || { echoc "failed to cp "$proj".jar"; exit 1; }
    elif [[ $proj == "iwds-ui-jar" ]]; then
        cp -vf bin/proguard/obfuscated.jar ../$userlibs/iwds-service-ui-jar.jar  || { echoc "failed to cp "$proj".jar"; exit 1; }
    elif [[ $proj == "iwds" ]]; then
        cp -vf bin/proguard/obfuscated.jar ../$serverlibs/$proj.jar  || { echoc "failed to cp "$proj".jar"; exit 1; }
        cp -vrf libs/arm*                  ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
        cp -vrf libs/mips*                 ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
        cp -vrf libs/x86*                  ../$serverlibs  || { echoc "failed to copy file";  exit 1; }
    fi
  fi
  echo ""
done

rm -rf "$top_dir/.tmp"
mkdir "$top_dir/.tmp"
[[ "$MP" == "" ]] && MP=5

for proj in $SERVER_PROJECTS; do

    while true; do
    (( $(jobs -p | wc -l) < MP )) && break
      sleep 1
    done

   echoc "Ant Compiling $proj in $buildmode mode"
   echoc "delete old .jar, copy new file to $proj libs"
   cd "$top_dir"
   mkdir -p $proj/libs
   rm -vf $proj/libs/iwds*.jar
   find $proj/libs -type f -name "libiwds.so" | xargs -i rm -vf {}
   cp -vrf JavaDocAndBuildOut/iwds-api*/server/libs/*  $proj/libs || { echoc "failed to copy libs file"; kill $top_pid; exit 1; }
   cp -vf AntBuildConfig/project-unuse-res.properties  $proj/project.properties || { echoc "failed to copy file"; kill $top_pid; exit 1; }
   if [[ -d "keystore" ]]; then
      echoc "copy keystore to $proj project"
      if [[ $proj == "watch-manager" ]]; then
         cp -vf  keystore/ingenic.keystore  $proj/ingenic.keystore || { echoc "failed to copy file"; kill $top_pid; exit 1; }
         cp -vf  keystore/ant.properties    $proj/ant.properties   || { echoc "failed to copy file"; kill $top_pid; exit 1; }
      fi
      if [[ "$proj" == "iwds-device" ]]; then
        cp -vf keystore/ingenicPlatform.keystore $proj/ingenicPlatform.keystore || { echoc "failed to copy file"; kill $top_pid; exit 1; }
        cp -vf keystore/ingenicPlatform-ant.properties $proj/ant.properties || { echoc "failed to copy file"; kill $top_pid; exit 1; }
      fi
   fi

   cd $proj/ || { echoc "directory $proj does not exist"; kill $top_pid; exit 1; }
   if [[ $buildmode == "debug" ]]; then
      ant elf-debug > "$top_dir/.tmp/$proj.log" 2>&1   || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
      echoc "  compile $proj done"
   else
      ant elf-release > "$top_dir/.tmp/$proj.log" 2>&1 || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
      echoc "  compile $proj done"
   fi &
done

echoc "wait......."
wait

cd "$top_dir/.tmp/"
for f in *; do
  echoc "dump $f:"
  cat "$f"
  echo ""
done

cd "$top_dir"

echoc "cp iwds-ui-res-jar source to "$iwds_api_outpath"/user"
cp -rf iwds-ui-res-jar $iwds_api_outpath/user/.
rm -rf $iwds_api_outpath/user/iwds-ui-res-jar/.git

echoc "invoke ./AntBuildConfig/elf-release-build.sh $1"
./AntBuildConfig/elf-release-build.sh $1 || { echoc "elf-release-build.sh failed."; exit 1;}

echoc "create java doc ..."
makejavadoc || { echoc "makejavadoc failed."; exit 1;}

echoc "build elf-demo ..."
./AntBuildConfig/elf-demobuild.sh $1 || { echoc "elf-demobuild.sh failed."; exit 1;}

trap - INT TERM EXIT
echoc "$0 done."

