#!/bin/bash

#--------watch app---------#
PROJECTS="
WatchConnector
FileTransactionTest
ParcelTransactorTest
test-notification-proxy-service-backend
ConnectionTest
iwds-api-demo
RemoteDeviceServiceTest
RemoteSensorTest
test-notification-proxy-service-frontend
DataTransactorTest
RemoteLocationTest
SensorTest
"

echoc()
{
  echo -e "\e[0;92m$1\e[0m"
}

echoc "Compiling apk ..."
ANT_PROJ="ant project"
which busybox > /dev/null && BUSYBOX="busybox" # `realpath' not default installed on many distribution
top_dir="$($BUSYBOX realpath "$(dirname "${BASH_SOURCE[0]}")/..")"

cd "$top_dir"

# check env, TODO: recheck the checking method
if [[ "$NDK_HOME" == "" || "$ANDROID_HOME" == "" ]]; then
    echo '
environment check error, FYR:

  export NDK_HOME=<path-to-android-ndk>
  export ANDROID_HOME=<path-to-android-sdk>

can be added to your profile or .bashrc
'
    exit 1
fi
# check build command
if [[ "$1" != "release" && "$1" != "debug" &&  "$1" != "clean" && "$1" != "force" && "$1" != "help" ]]; then
echo '
  command error,please execute the command correctly.
  example:
  ./AntBuildConfig/elf-demobuild.sh clean
  ./AntBuildConfig/elf-demobuild.sh debug
  ./AntBuildConfig/elf-demobuild.sh release
  ./AntBuildConfig/elf-demobuild.sh help
'
    exit 1
fi

# help
if [[ "$1" == help ]]; then
echo '
  ./AntBuildConfig/elf-demobuild.sh clean :       Removes output files created by other targets.
  ./AntBuildConfig/elf-demobuild.sh debug :       Builds the application in debug mode.
  ./AntBuildConfig/elf-demobuild.sh release :     Builds the application in release mode.
  ./AntBuildConfig/elf-demobuild.sh help :        help
'
   exit 1;
fi

# clean, TODO: warning before clean, or do the cleanup work using the smk only
if [ "$1" == "force" ]; then
  echoc "start repo sync elf source."
  repo sync > /dev/null
  repo forall -c 'git reset --hard' > /dev/null
  repo forall -c 'git clean -dxf' > /dev/null
  echoc "repo sync done."
fi

cd "$top_dir"
# clean
if [ "$1" == "clean" ]; then
  echoc "Ant clean begining"
  for proj in $PROJECTS; do
     cd "$top_dir/elf-demo"
     echoc "Ant clean $proj"

     cd $proj/ || { echoc "directory $proj does not exist"; exit 1; }
     ant clean || { echoc "failed to ant clean $proj"; exit 1; }
     echo ""
  done
  echoc "Ant clean end."
  exit 0
fi


export top_pid=$$
function stop_all_job {
  echo "$0 kill all background jobs before exit, pid=$top_pid"
  pkill -P $top_pid
  echo "$0 exit..."
  exit 1
}
trap stop_all_job INT TERM EXIT

# build
buildmode=$1
rm -rf "$top_dir/.tmp"
mkdir "$top_dir/.tmp"
[[ "$MP" == "" ]] && MP=5

for proj in $PROJECTS; do

  while true; do
    (( $(jobs -p | wc -l) < MP )) && break
    sleep 1
  done

  cd "$top_dir/samples"
  echoc "Ant Compiling $proj $buildmode"

  mkdir -p $proj/libs
  rm -f $proj/libs/iwds*.jar
  cp -vrf ../AntBuildConfig/project-use-res.properties $proj/project.properties || { echoc "failed to copy libs file"; kill $top_pid; exit 1; }
  if [[ -d "$proj/iwds-ui-res-jar" ]]; then
     echoc "delete $proj/iwds-ui-res-jar"
     rm -rf $proj/iwds-ui-res-jar
  fi
  echoc "cp iwds-ui-res-jar to $proj"
  cp -rf ../iwds-ui-res-jar  $proj/.
  rm -rf $proj/iwds-ui-res-jar/.git

  if [[ "$proj" == "WatchConnector" ]]; then
     cp -vrf ../JavaDocAndBuildOut/iwds-api*/server/libs/*  $proj/libs || { echoc "failed to copy libs file"; kill $top_pid; exit 1; }
  else
     cp -vrf ../JavaDocAndBuildOut/iwds-api*/user/libs/*    $proj/libs/              || { echoc "failed to copy file"; kill $top_pid; exit 1; }
  fi

  if [[ ! "$proj" == "iwds-api-demo" ]]; then
     cp -vrf ../AntBuildConfig/project-unuse-res.properties $proj/project.properties || { echoc "failed to copy libs file"; kill $top_pid; exit 1; }
     rm -rf $proj/iwds-ui-res-jar
     if [[ $buildmode == "debug" ]]; then
	rm $proj/libs/iwds-ui-jar.jar
        rm $proj/libs/iwds-ui-with-res-jar.jar
     fi
     if [[ $buildmode == "release" ]]; then
        rm $proj/libs/iwds-ui-with-res-jar.jar
     fi
  fi

 cd $proj/ || { echoc "directory $proj does not exist"; kill $top_pid; exit 1; }
  if [[ $buildmode == "debug" ]]; then
    ant elf-debug > "$top_dir/.tmp/$proj.log" 2>&1   || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
    echoc "  compile $proj done"
  else
    ant elf-release > "$top_dir/.tmp/$proj.log" 2>&1 || { echoc "failed to compile $proj"; cat "$top_dir/.tmp/$proj.log"; kill $top_pid; exit 1; }
    echoc "  compile $proj done"
  fi &

done

echoc "wait......."
wait

cd "$top_dir/.tmp/"
for f in *; do
  echoc "dump $f:"
  cat "$f"
  echo ""
done

cd $top_dir

echoc "copy samples to JavaDocAndBuildOut/demo"
mkdir -p JavaDocAndBuildOut/demo/samples

for proj in $PROJECTS; do
  cd "$top_dir/samples"
  echoc "delete $proj bin & gen"
  rm -rf "$proj/bin"
  rm -rf "$proj/gen"
  cp -rf "$proj" ../JavaDocAndBuildOut/demo/samples
done
cd $top_dir
trap - INT TERM EXIT
echoc "$0 done."

